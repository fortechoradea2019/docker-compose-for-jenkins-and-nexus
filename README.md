#  License Manager - docker-compose for Jenkins and Nexus

## If you want to run the Jenkins and Nexus, you must follow the next steps:

### 1.Go into docker-compose-for-Jenkins-and-Nexus and run the following comand:
`docker-compose up --build`

### 2.Jenkins Configuration is set in the docker compose file to run on port 8082 therefore if we visit http://localhost:8082 we will be greeted with a screen like this:

![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/7093d7e4cc6b250f9e87302176a244e485f5e7c8/Docs/unlock-jenkins.png)

### 3.Enter the key, you will find the key on terminal or in jenkins/var/jenkins_home/secrets/initialAdminPassword
![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/7093d7e4cc6b250f9e87302176a244e485f5e7c8/Docs/Screenshot1.png)

### 4.After entering the password, we will download recommended plugins and define an admin user.

![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/7093d7e4cc6b250f9e87302176a244e485f5e7c8/Docs/create-admin-user.png)

### 5.After clicking Save and Finish and Start using Jenkins buttons, we should be seeing the Jenkins homepage.

![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/7093d7e4cc6b250f9e87302176a244e485f5e7c8/Docs/jenkins-homepage.png)


## Nexus configuration

### 1. After run: `docker-compose up` go to browser at: `localhost:8081` and log-in with username:admin and password:admin123

![home](Docs/loginexus.png)


## Continuous integration

### 1. Before you create a job you have to set credential for nexus, so you have to go to Credentials, System and click on Global credentials
![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/a2ecb30ca8ebaec84416132fccec348e716020ef/Docs/Screenshot%20from%202019-02-11%2013-43-04.png)
### 2. Add new credential
![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/a2ecb30ca8ebaec84416132fccec348e716020ef/Docs/Screenshot%20from%202019-02-11%2013-43-31.png)
### 3. Set the username: admin, password: admin123 and ID: nexus-2 . After click ok
![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/a2ecb30ca8ebaec84416132fccec348e716020ef/Docs/Screenshot%20from%202019-02-11%2013-44-23.png)
### 4. You have to configure global tools, go to manage Jenkins and click on Global Tool Configuration
![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/a2ecb30ca8ebaec84416132fccec348e716020ef/Docs/Screenshot%20from%202019-02-11%2010-33-27.png)
### 5.Click on Add Maven and set the name Maven and the same way for NodeJS  set the name NodeJS
![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/a2ecb30ca8ebaec84416132fccec348e716020ef/Docs/Screenshot%20from%202019-02-11%2010-34-18.png)
### 6. Go and make a new job

![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/aa446c4223caf8a5805209bc9c08ec08da23b827/Docs/Screenshot%20from%202019-02-08%2016-35-21.png)

### 7. Give a name and Choose pipeline
![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/aa446c4223caf8a5805209bc9c08ec08da23b827/Docs/Screenshot%20from%202019-02-08%2016-36-22.png)

### 8. check the `Build when a change is pushed to BitBucket`

![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/aa446c4223caf8a5805209bc9c08ec08da23b827/Docs/Screenshot%20from%202019-02-08%2016-36-50.png)

### 9.Choose for the pipelane definition Pipelane script from SCM

![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/aa446c4223caf8a5805209bc9c08ec08da23b827/Docs/Screenshot%20from%202019-02-08%2016-38-04.png)

### 10. Put all the repositories and Save

![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/aa446c4223caf8a5805209bc9c08ec08da23b827/Docs/Screenshot%20from%202019-02-08%2016-38-48.png)

### 11. Install ngrok on your local machine and execute the following command
`sudo apt-get install ngrok`
`ngrok http 8082`

![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/aa446c4223caf8a5805209bc9c08ec08da23b827/Docs/Screenshot%20from%202019-02-08%2016-41-04.png)

###  12. Goo to bitbucket and for all repositories execute the next steps

![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/aa446c4223caf8a5805209bc9c08ec08da23b827/Docs/Screenshot%20from%202019-02-08%2016-39-28.png)

### 13. Goo to Settings and Webhooks

![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/aa446c4223caf8a5805209bc9c08ec08da23b827/Docs/Screenshot%20from%202019-02-08%2016-40-02.png)

### 14. Add new Webhook with url from ngrock
`your_url/bitbucket-hook/`

![alt text](https://bitbucket.org/fortechoradea2019/docker-compose-for-jenkins-and-nexus/raw/aa446c4223caf8a5805209bc9c08ec08da23b827/Docs/Screenshot%20from%202019-02-08%2016-41-39.png)




